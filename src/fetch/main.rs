use std::thread;
use std::time::Instant;

#[macro_use]
extern crate log;

use dotenv::dotenv;
use reqwest;
use scraper::{Html, Selector};

use db::models::{QbStats, RbStats, TeStats, WrStats};
use db::shared::positions::Position;

fn fetch_position_stats_html_string(position: Position) -> String {
    info!(
        "generating position stats url for position: {}",
        position.to_string()
    );
    let position = match position {
        Position::QB => "QUARTERBACK",
        Position::RB => "RUNNING_BACK",
        Position::WR => "WIDE_RECEIVER",
        Position::TE => "TIGHT_END",
    };

    // TODO: Allow this to let me try pages?
    let url = format!(
        "http://www.nfl.com/stats/categorystats?tabSeq=1&statisticPositionCategory={}",
        position
    );

    info!("fetching data at url: {}", url);

    let mut response = reqwest::get(&url).unwrap();
    response.text().unwrap()
}

fn get_and_store_position_results_table(html: &str, position: Position) {
    let parsed_html = Html::parse_document(html);
    let table_selector = Selector::parse("#result").unwrap();

    let result_table = parsed_html.select(&table_selector).next().unwrap();

    info!(
        "Starting parsing and writing results to the DB for position: {}",
        position.to_string()
    );
    match position {
        Position::QB => {
            let stats = parse_stats(&result_table, &mut QbStats::default());
            info!(
                "{} records found for position: {}, storing in DB now",
                stats.len(),
                position.to_string()
            );

            let conn = db::establish_connection();
            for stat in stats {
                db::insert_qb_stats_db(&conn, stat);
            }
        }
        Position::RB => {
            let stats = parse_stats(&result_table, &mut RbStats::default());
            info!(
                "{} records found for position: {}, storing in DB now",
                stats.len(),
                position.to_string()
            );

            let conn = db::establish_connection();
            for stat in stats {
                db::insert_rb_stats_db(&conn, stat);
            }
        }
        Position::WR => {
            let stats = parse_stats(&result_table, &mut WrStats::default());
            info!(
                "{} records found for position: {}, storing in DB now",
                stats.len(),
                position.to_string()
            );

            let conn = db::establish_connection();
            for stat in stats {
                db::insert_wr_stats_db(&conn, stat);
            }
        }
        Position::TE => {
            let stats = parse_stats(&result_table, &mut TeStats::default());
            info!(
                "{} records found for position: {}, storing in DB now",
                stats.len(),
                position.to_string()
            );

            let conn = db::establish_connection();
            for stat in stats {
                db::insert_te_stats_db(&conn, stat);
            }
        }
    }

    info!(
        "Finished storing results in DB for position: {}",
        position.to_string()
    );
}

fn parse_stats<T>(result_table: &scraper::element_ref::ElementRef, stats: &mut T) -> Vec<T>
where
    T: db::models::Mappable + std::clone::Clone,
{
    let row_selector = Selector::parse("tr").unwrap();
    let selector = Selector::parse("td").unwrap();
    let mut all_stats: Vec<T> = Vec::new();

    //Skipping the first one as it is the table header row (not sure why they didn't use th...)
    for row in result_table.select(&row_selector).skip(1) {
        for (i, elem) in row.select(&selector).enumerate() {
            let value = strip_tab_newline(&elem.inner_html());
            stats.mapper(&value, i);
        }
        all_stats.push(stats.clone());
    }

    all_stats
}

fn strip_tab_newline(elem: &str) -> String {
    elem.replace("\t", "").replace("\n", "")
}

// TODO: Is threading or async / awaiting better?
fn fetch_and_store_stats() {
    info!("Starting to fetch all stats.");
    let mut threads = Vec::new();

    threads.push(thread::spawn(|| {
        info!("Quarterback thread started.");
        let qb_html = fetch_position_stats_html_string(Position::QB);
        get_and_store_position_results_table(&qb_html, Position::QB);
        info!("Quarterback thread logic finished.");
    }));

    threads.push(thread::spawn(|| {
        info!("Running Back thread started.");
        let rb_html = fetch_position_stats_html_string(Position::RB);
        get_and_store_position_results_table(&rb_html, Position::RB);
        info!("Running Back thread logic finished.");
    }));

    threads.push(thread::spawn(|| {
        info!("Wide Receiver thread started.");
        let wr_html = fetch_position_stats_html_string(Position::WR);
        get_and_store_position_results_table(&wr_html, Position::WR);
        info!("Wide Receiver thread logic finished.");
    }));

    threads.push(thread::spawn(|| {
        info!("Tight End thread started.");
        let te_stats = fetch_position_stats_html_string(Position::TE);
        get_and_store_position_results_table(&te_stats, Position::TE);
        info!("Tight End thread logic finished.");
    }));

    for thread in threads {
        thread.join().unwrap();
    }

    info!("All threads are completed.");
}

fn main() {
    dotenv().ok();
    env_logger::init();

    let start = Instant::now();
    info!("Starting NFL data fetch.");

    info!("Fetching and storing stats...");
    // Fetch stats to place in the DB
    fetch_and_store_stats();
    let duration = start.elapsed();

    info!("NFL data fetch completed.  Total time: {:?}", duration);
}
