pub mod shared;
pub mod models;
pub mod schema;

#[macro_use]
extern crate diesel;
#[macro_use]
extern crate lazy_static;

use std::env;

use diesel::prelude::*;
use diesel::pg::PgConnection;
use dotenv::dotenv;

use self::models::{QbStats, QbStatsModel, WrStats, WrStatsModel, TeStats, TeStatsModel, RbStats, RbStatsModel};


pub fn establish_connection() -> PgConnection {
    dotenv().ok();

    let database_url = env::var("DATABASE_URL")
        .expect("DATABASE_URL must be set");
    PgConnection::establish(&database_url)
        .unwrap_or_else(|_| panic!("Error connecting to {}", database_url))
}

pub fn insert_qb_stats_db(conn: &PgConnection, stats: QbStats) -> QbStatsModel {
    use schema::qb_total_stats;

    diesel::insert_into(qb_total_stats::table)
        .values(&stats)
        .get_result(conn)
        .expect("Error saving QB State to qb_total_stats")
}

pub fn fetch_all_qb_stats() -> Vec<QbStatsModel> {
    use schema::qb_total_stats::dsl::*;

    // TODO - Need to create a connection pool that is added to the state of the request instead of fetching this always...
    let connection = establish_connection();
    qb_total_stats
        .load::<QbStatsModel>(&connection)
        .expect("Error loading the QB stats")
}

pub fn insert_rb_stats_db(conn: &PgConnection, stats: RbStats) -> RbStatsModel {
    use schema::rb_total_stats;

    diesel::insert_into(rb_total_stats::table)
        .values(&stats)
        .get_result(conn)
        .expect("Error saving RB State to qb_total_stats")
}

pub fn fetch_all_rb_stats() -> Vec<RbStatsModel> {
    use schema::rb_total_stats::dsl::*;

    // TODO - Need to create a connection pool that is added to the state of the request instead of fetching this always...
    let connection = establish_connection();
    rb_total_stats
        .load::<RbStatsModel>(&connection)
        .expect("Error loading the RB stats")
}

pub fn insert_te_stats_db(conn: &PgConnection, stats: TeStats) -> TeStatsModel {
    use schema::te_total_stats;

    diesel::insert_into(te_total_stats::table)
        .values(&stats)
        .get_result(conn)
        .expect("Error saving TE State to qb_total_stats")
}

pub fn fetch_all_te_stats() -> Vec<TeStatsModel> {
    use schema::te_total_stats::dsl::*;

    // TODO - Need to create a connection pool that is added to the state of the request instead of fetching this always...
    let connection = establish_connection();
    te_total_stats
        .load::<TeStatsModel>(&connection)
        .expect("Error loading the TE stats")
}

pub fn insert_wr_stats_db(conn: &PgConnection, stats: WrStats) -> WrStatsModel {
    use schema::wr_total_stats;

    diesel::insert_into(wr_total_stats::table)
        .values(&stats)
        .get_result(conn)
        .expect("Error saving WR State to qb_total_stats")
}

pub fn fetch_all_wr_stats() -> Vec<WrStatsModel> {
    use schema::wr_total_stats::dsl::*;

    // TODO - Need to create a connection pool that is added to the state of the request instead of fetching this always...
    let connection = establish_connection();
    wr_total_stats
        .load::<WrStatsModel>(&connection)
        .expect("Error loading the WR stats")
}