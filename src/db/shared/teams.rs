use std::str::FromStr;
use std::string::ToString;
use std::io::Write;

use diesel::backend::Backend;
use diesel::sql_types::Text;
use diesel::deserialize::{self, FromSql};
use diesel::serialize::{self, ToSql, Output};
use serde::Serialize;


#[derive(Debug, Copy, Clone, AsExpression, FromSqlRow, Serialize)]
#[sql_type = "Text"]
pub enum Team {
    ARI,
    ATL,
    BAL,
    BUF,
    CAR,
    CHI,
    CIN,
    CLE,
    DAL,
    DEN,
    DET,
    GB,
    HOU,
    IND,
    JAX,
    KC,
    LA,
    LAC,
    MIA,
    MIN,
    NE,
    NO,
    NYG,
    NYJ,
    OAK,
    PHI,
    PIT,
    SEA,
    SF,
    TB,
    TEN,
    WAS,
}

impl<DB> ToSql<Text, DB> for Team
where
    DB: Backend,
    String: ToSql<Text, DB>,
{
    fn to_sql<W: Write>(&self, out: &mut Output<W, DB>) -> serialize::Result {
        let output = self.to_string();
        output.to_sql(out)
    }
}

impl<DB> FromSql<Text, DB> for Team
where
    DB: Backend,
    String: FromSql<Text, DB>,
{
    fn from_sql(bytes: Option<&DB::RawValue>) -> deserialize::Result<Self> {
        let v = String::from_sql(bytes)?;
        Ok(match v.as_str() {
            "ARI" => Team::ARI,
            "ATL" => Team::ATL,
            "BAL" => Team::BAL,
            "BUF" => Team::BUF,
            "CAR" => Team::CAR,
            "CHI" => Team::CHI,
            "CIN" => Team::CIN,
            "CLE" => Team::CLE,
            "DAL" => Team::DAL,
            "DEN" => Team::DEN,
            "DET" => Team::DET,
            "GB" => Team::GB,
            "HOU" => Team::HOU,
            "IND" => Team::IND,
            "JAX" => Team::JAX,
            "KC" => Team::KC,
            "LA" => Team::LA,
            "LAC" => Team::LAC,
            "MIA" => Team::MIA,
            "MIN" => Team::MIN,
            "NE" => Team::NE,
            "NO" => Team::NO,
            "NYG" => Team::NYG,
            "NYJ" => Team::NYJ,
            "OAK" => Team::OAK,
            "PHI" => Team::PHI,
            "PIT" => Team::PIT,
            "SEA" => Team::SEA,
            "SF" => Team::SF,
            "TB" => Team::TB,
            "TEN" => Team::TEN,
            "WAS" => Team::WAS,
            _ => return Err("Cannot deserialize the Team from DB into Team Enum".into()),
        })
    }
}

impl FromStr for Team {
    type Err = ();

    fn from_str(s: &str) -> Result<Team, ()> {
        match s {
            "ARI" => Ok(Team::ARI),
            "ATL" => Ok(Team::ATL),
            "BAL" => Ok(Team::BAL),
            "BUF" => Ok(Team::BUF),
            "CAR" => Ok(Team::CAR),
            "CHI" => Ok(Team::CHI),
            "CIN" => Ok(Team::CIN),
            "CLE" => Ok(Team::CLE),
            "DAL" => Ok(Team::DAL),
            "DEN" => Ok(Team::DEN),
            "DET" => Ok(Team::DET),
            "GB" => Ok(Team::GB),
            "HOU" => Ok(Team::HOU),
            "IND" => Ok(Team::IND),
            "JAX" => Ok(Team::JAX),
            "KC" => Ok(Team::KC),
            "LA" => Ok(Team::LA),
            "LAC" => Ok(Team::LAC),
            "MIA" => Ok(Team::MIA),
            "MIN" => Ok(Team::MIN),
            "NE" => Ok(Team::NE),
            "NO" => Ok(Team::NO),
            "NYG" => Ok(Team::NYG),
            "NYJ" => Ok(Team::NYJ),
            "OAK" => Ok(Team::OAK),
            "PHI" => Ok(Team::PHI),
            "PIT" => Ok(Team::PIT),
            "SEA" => Ok(Team::SEA),
            "SF" => Ok(Team::SF),
            "TB" => Ok(Team::TB),
            "TEN" => Ok(Team::TEN),
            "WAS" => Ok(Team::WAS),
            _ => Err(()),
        }
    }
}

impl ToString for Team {
    fn to_string(&self) -> String {
        let result = match self {
            Team::ARI => "ARI",
            Team::ATL => "ATL",
            Team::BAL => "BAL",
            Team::BUF => "BUF",
            Team::CAR => "CAR",
            Team::CHI => "CHI",
            Team::CIN => "CIN",
            Team::CLE => "CLE",
            Team::DAL => "DAL",
            Team::DEN => "DEN",
            Team::DET => "DET",
            Team::GB => "GB",
            Team::HOU => "HOU",
            Team::IND => "IND",
            Team::JAX => "JAX",
            Team::KC => "KC",
            Team::LA => "LA",
            Team::LAC => "LAC",
            Team::MIA => "MIA",
            Team::MIN => "MIN",
            Team::NE => "NE",
            Team::NO => "NO",
            Team::NYG => "NYG",
            Team::NYJ => "NYJ",
            Team::OAK => "OAK",
            Team::PHI => "PHI",
            Team::PIT => "PIT",
            Team::SEA => "SEA",
            Team::SF => "SF",
            Team::TB => "TB",
            Team::TEN => "TEN",
            Team::WAS => "WAS",
        };

        result.to_owned()
    }
}
