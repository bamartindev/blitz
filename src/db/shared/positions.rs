pub enum Position {
    QB,
    RB,
    WR,
    TE,
}

impl ToString for Position {
    fn to_string(&self) -> String {
        let result = match self {
            Self::QB => "Quarterback",
            Self::RB => "Running Back",
            Self::WR => "Wide Receiver",
            Self::TE => "Tight End",
        };

        result.to_owned()
    }
}