use regex::Regex;

// Good enough, but can probably make a better regex when I learn more :)
pub fn parse_players_team(text: &str) -> String {
    lazy_static! {
        static ref RE: Regex = Regex::new(r">([A-Z]+)</a>").unwrap();
    }

    let cap = RE.captures(text).unwrap();
    cap[1].to_owned()
}

pub fn parse_player_name(text: &str) -> String {
    lazy_static! {
        static ref RE: Regex = Regex::new(r">([A-Za-z .'-]+)</a>").unwrap();
    }
    let cap = RE
        .captures(text)
        .unwrap_or_else(|| panic!("Failed to parse player name from str: {}", text));
    cap[1].to_owned()
}
