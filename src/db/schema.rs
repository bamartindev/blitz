table! {
    qb_total_stats (id) {
        id -> Int4,
        rank -> Nullable<Int2>,
        player -> Nullable<Varchar>,
        team -> Nullable<Varchar>,
        passing_completions -> Nullable<Int2>,
        passing_attempts -> Nullable<Int2>,
        passing_pct -> Nullable<Float4>,
        avg_attempts_per_game -> Nullable<Float4>,
        passing_yards -> Nullable<Int2>,
        avg_passing_yards -> Nullable<Float4>,
        avg_yards_per_game -> Nullable<Float4>,
        passing_touchdowns -> Nullable<Int2>,
        interceptions -> Nullable<Int2>,
        passing_first_downs -> Nullable<Int2>,
        passing_first_down_pct -> Nullable<Float4>,
        passing_long -> Nullable<Varchar>,
        twenty_plus_pass -> Nullable<Int2>,
        forty_plus_pass -> Nullable<Int2>,
        sack_count -> Nullable<Int2>,
        passer_rating -> Nullable<Float4>,
        inserted_ts -> Nullable<Timestamp>,
    }
}

table! {
    rb_total_stats (id) {
        id -> Int4,
        rank -> Nullable<Int2>,
        player -> Nullable<Varchar>,
        team -> Nullable<Varchar>,
        rushing_attempts -> Nullable<Int2>,
        avg_rushing_attempts_per_game -> Nullable<Float4>,
        rushing_yards -> Nullable<Int2>,
        avg_rushing_yards -> Nullable<Float4>,
        avg_rushing_yards_per_game -> Nullable<Float4>,
        rushing_touchdowns -> Nullable<Int2>,
        rushing_long -> Nullable<Varchar>,
        rushing_first_downs -> Nullable<Int2>,
        rushing_first_down_pct -> Nullable<Float4>,
        twenty_plus_rush -> Nullable<Int2>,
        forty_plus_rush -> Nullable<Int2>,
        rushing_fumbles -> Nullable<Int2>,
        inserted_ts -> Nullable<Timestamp>,
    }
}

table! {
    te_total_stats (id) {
        id -> Int4,
        rank -> Nullable<Int2>,
        player -> Nullable<Varchar>,
        team -> Nullable<Varchar>,
        receiving_receptions -> Nullable<Int2>,
        receiving_yards -> Nullable<Int2>,
        avg_receiving_yards -> Nullable<Float4>,
        avg_receiving_yards_per_game -> Nullable<Float4>,
        receiving_touchdowns -> Nullable<Int2>,
        receiving_long -> Nullable<Varchar>,
        receiving_first_downs -> Nullable<Int2>,
        receiving_first_down_pct -> Nullable<Float4>,
        twenty_plus_receiving -> Nullable<Int2>,
        forty_plus_receiving -> Nullable<Int2>,
        receiving_fumbles -> Nullable<Int2>,
        inserted_ts -> Nullable<Timestamp>,
    }
}

table! {
    wr_total_stats (id) {
        id -> Int4,
        rank -> Nullable<Int2>,
        player -> Nullable<Varchar>,
        team -> Nullable<Varchar>,
        receiving_receptions -> Nullable<Int2>,
        receiving_yards -> Nullable<Int2>,
        avg_receiving_yards -> Nullable<Float4>,
        avg_receiving_yards_per_game -> Nullable<Float4>,
        receiving_touchdowns -> Nullable<Int2>,
        receiving_long -> Nullable<Varchar>,
        receiving_first_downs -> Nullable<Int2>,
        receiving_first_down_pct -> Nullable<Float4>,
        twenty_plus_receiving -> Nullable<Int2>,
        forty_plus_receiving -> Nullable<Int2>,
        receiving_fumbles -> Nullable<Int2>,
        inserted_ts -> Nullable<Timestamp>,
    }
}

allow_tables_to_appear_in_same_query!(
    qb_total_stats,
    rb_total_stats,
    te_total_stats,
    wr_total_stats,
);
