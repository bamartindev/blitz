use std::time::SystemTime;

use serde::Serialize;

use super::schema::{qb_total_stats, rb_total_stats, te_total_stats, wr_total_stats};
use super::shared::teams::Team;
use super::shared::parsers::{parse_players_team, parse_player_name};

pub trait Mappable {
    fn mapper(&mut self, val: &str, stat: usize);
}

#[derive(Debug, Clone, Insertable)]
#[table_name="qb_total_stats"]
pub struct QbStats {
    pub rank: Option<i16>,
    pub player: Option<String>,
    pub team: Option<Team>,
    pub passing_completions: Option<i16>,
    pub passing_attempts: Option<i16>,
    pub passing_pct: Option<f32>,
    pub avg_attempts_per_game: Option<f32>,
    pub passing_yards: Option<i16>,
    pub avg_passing_yards: Option<f32>,
    pub avg_yards_per_game: Option<f32>,
    pub passing_touchdowns: Option<i16>,
    pub interceptions: Option<i16>,
    pub passing_first_downs: Option<i16>,
    pub passing_first_down_pct: Option<f32>,
    pub passing_long: Option<String>,
    pub twenty_plus_pass: Option<i16>,
    pub forty_plus_pass: Option<i16>,
    pub sack_count: Option<i16>,
    pub passer_rating: Option<f32>,
}

impl Default for QbStats {
    fn default() -> Self {
        Self {
            rank: None,
            player: None,
            team: None,
            passing_completions: None,
            passing_attempts: None,
            passing_pct: None,
            avg_attempts_per_game: None,
            passing_yards: None,
            avg_passing_yards: None,
            avg_yards_per_game: None,
            passing_touchdowns: None,
            interceptions: None,
            passing_first_downs: None,
            passing_first_down_pct: None,
            passing_long: None,
            twenty_plus_pass: None,
            forty_plus_pass: None,
            sack_count: None,
            passer_rating: None,
        }
    }
}

impl Mappable for QbStats {
    fn mapper(&mut self, val: &str, stat: usize) {
        match stat {
            0 => self.rank = Some(val.parse::<i16>().unwrap()),
            1 => self.player = Some(parse_player_name(val)),
            2 => self.team = Some(parse_players_team(val).parse::<Team>().unwrap()),
            3 => (), // Do nothing - this is just the position, which is redundant.
            4 => self.passing_completions = Some(val.parse::<i16>().unwrap()),
            5 => self.passing_attempts = Some(val.parse::<i16>().unwrap()),
            6 => self.passing_pct = Some(val.parse::<f32>().unwrap()),
            7 => self.avg_attempts_per_game = Some(val.parse::<f32>().unwrap()),
            8 => self.passing_yards = Some(val.replace(",", "").parse::<i16>().unwrap()),
            9 => self.avg_passing_yards = Some(val.parse::<f32>().unwrap()),
            10 => self.avg_yards_per_game = Some(val.parse::<f32>().unwrap()),
            11 => self.passing_touchdowns = Some(val.parse::<i16>().unwrap()),
            12 => self.interceptions = Some(val.parse::<i16>().unwrap()),
            13 => self.passing_first_downs = Some(val.parse::<i16>().unwrap()),
            14 => self.passing_first_down_pct = Some(val.parse::<f32>().unwrap()),
            15 => self.passing_long = Some(val.to_owned()),
            16 => self.twenty_plus_pass = Some(val.parse::<i16>().unwrap()),
            17 => self.forty_plus_pass = Some(val.parse::<i16>().unwrap()),
            18 => self.sack_count = Some(val.parse::<i16>().unwrap()),
            19 => self.passer_rating = Some(val.parse::<f32>().unwrap()),
            _ => unimplemented!(),
        }
    }
}

#[derive(Queryable, Debug, Serialize)]
pub struct QbStatsModel {
    pub id: i32,
    pub rank: Option<i16>,
    pub player: Option<String>,
    pub team: Option<Team>,
    pub passing_completions: Option<i16>,
    pub passing_attempts: Option<i16>,
    pub passing_pct: Option<f32>,
    pub avg_attempts_per_game: Option<f32>,
    pub passing_yards: Option<i16>,
    pub avg_passing_yards: Option<f32>,
    pub avg_yards_per_game: Option<f32>,
    pub passing_touchdowns: Option<i16>,
    pub interceptions: Option<i16>,
    pub passing_first_downs: Option<i16>,
    pub passing_first_down_pct: Option<f32>,
    pub passing_long: Option<String>,
    pub twenty_plus_pass: Option<i16>,
    pub forty_plus_pass: Option<i16>,
    pub sack_count: Option<i16>,
    pub passer_rating: Option<f32>,
    pub inserted_ts: Option<SystemTime>,
}

#[derive(Debug, Clone, Insertable)]
#[table_name="rb_total_stats"]
pub struct RbStats {
    pub rank: Option<i16>,
    pub player: Option<String>,
    pub team: Option<Team>,
    pub rushing_attempts: Option<i16>,
    pub avg_rushing_attempts_per_game: Option<f32>,
    pub rushing_yards: Option<i16>,
    pub avg_rushing_yards: Option<f32>,
    pub avg_rushing_yards_per_game: Option<f32>,
    pub rushing_touchdowns: Option<i16>,
    pub rushing_long: Option<String>,
    pub rushing_first_downs: Option<i16>,
    pub rushing_first_down_pct: Option<f32>,
    pub twenty_plus_rush: Option<i16>,
    pub forty_plus_rush: Option<i16>,
    pub rushing_fumbles: Option<i16>,
}

impl Default for RbStats {
    fn default() -> Self {
        Self {
            rank: None,
            player: None,
            team: None,
            rushing_attempts: None,
            avg_rushing_attempts_per_game: None,
            rushing_yards: None,
            avg_rushing_yards: None,
            avg_rushing_yards_per_game: None,
            rushing_touchdowns: None,
            rushing_long: None,
            rushing_first_downs: None,
            rushing_first_down_pct: None,
            twenty_plus_rush: None,
            forty_plus_rush: None,
            rushing_fumbles: None,
        }
    }
}

impl Mappable for RbStats {
    fn mapper(&mut self, val: &str, stat: usize) {
        match stat {
            0 => self.rank = Some(val.parse::<i16>().unwrap()),
            1 => self.player = Some(parse_player_name(val)),
            2 => self.team = Some(parse_players_team(val).parse::<Team>().unwrap()),
            3 => (), // Do nothing - this is just the position, which is redundant.
            4 => self.rushing_attempts = Some(val.parse::<i16>().unwrap()),
            5 => self.avg_rushing_attempts_per_game = Some(val.parse::<f32>().unwrap()),
            6 => self.rushing_yards = Some(val.replace(",", "").parse::<i16>().unwrap()),
            7 => self.avg_rushing_yards = Some(val.parse::<f32>().unwrap()),
            8 => self.avg_rushing_yards_per_game = Some(val.parse::<f32>().unwrap()),
            9 => self.rushing_touchdowns = Some(val.parse::<i16>().unwrap()),
            10 => self.rushing_long = Some(val.parse::<String>().unwrap()),
            11 => self.rushing_first_downs = Some(val.parse::<i16>().unwrap()),
            12 => self.rushing_first_down_pct = Some(val.parse::<f32>().unwrap()),
            13 => self.twenty_plus_rush = Some(val.parse::<i16>().unwrap()),
            14 => self.forty_plus_rush = Some(val.parse::<i16>().unwrap()),
            15 => self.rushing_fumbles = Some(val.parse::<i16>().unwrap()),
            _ => unimplemented!(),
        }
    }
}

#[derive(Queryable, Serialize)]
pub struct RbStatsModel {
    pub id: i32,
    pub rank: Option<i16>,
    pub player: Option<String>,
    pub team: Option<Team>,
    pub rushing_attempts: Option<i16>,
    pub avg_rushing_attempts_per_game: Option<f32>,
    pub rushing_yards: Option<i16>,
    pub avg_rushing_yards: Option<f32>,
    pub avg_rushing_yards_per_game: Option<f32>,
    pub rushing_touchdowns: Option<i16>,
    pub rushing_long: Option<String>,
    pub rushing_first_downs: Option<i16>,
    pub rushing_first_down_pct: Option<f32>,
    pub twenty_plus_rush: Option<i16>,
    pub forty_plus_rush: Option<i16>,
    pub rushing_fumbles: Option<i16>,
    pub inserted_ts: Option<SystemTime>,
}

#[derive(Debug, Clone, Insertable)]
#[table_name="te_total_stats"]
pub struct TeStats {
    pub rank: Option<i16>,
    pub player: Option<String>,
    pub team: Option<Team>,
    pub receiving_receptions: Option<i16>,
    pub receiving_yards: Option<i16>,
    pub avg_receiving_yards: Option<f32>,
    pub avg_receiving_yards_per_game: Option<f32>,
    pub receiving_touchdowns: Option<i16>,
    pub receiving_long: Option<String>,
    pub receiving_first_downs: Option<i16>,
    pub receiving_first_down_pct: Option<f32>,
    pub twenty_plus_receiving: Option<i16>,
    pub forty_plus_receiving: Option<i16>,
    pub receiving_fumbles: Option<i16>,
}

impl Default for TeStats {
    fn default() -> Self {
        Self {
            rank: None,
            player: None,
            team: None,
            receiving_receptions: None,
            avg_receiving_yards: None,
            receiving_yards: None,
            avg_receiving_yards_per_game: None,
            receiving_touchdowns: None,
            receiving_long: None,
            receiving_first_downs: None,
            receiving_first_down_pct: None,
            twenty_plus_receiving: None,
            forty_plus_receiving: None,
            receiving_fumbles: None,
        }
    }
}

impl Mappable for TeStats {
    fn mapper(&mut self, val: &str, stat: usize) {
        match stat {
            0 => self.rank = Some(val.parse::<i16>().unwrap()),
            1 => self.player = Some(parse_player_name(val)),
            2 => self.team = Some(parse_players_team(val).parse::<Team>().unwrap()),
            3 => (), // Do nothing - this is just the position, which is redundant.
            4 => self.receiving_receptions = Some(val.parse::<i16>().unwrap()),
            5 => self.receiving_yards = Some(val.replace(",", "").parse::<i16>().unwrap()),
            6 => self.avg_receiving_yards = Some(val.parse::<f32>().unwrap()),
            7 => self.avg_receiving_yards_per_game = Some(val.parse::<f32>().unwrap()),
            8 => self.receiving_long = Some(val.parse::<String>().unwrap()),
            9 => self.receiving_touchdowns = Some(val.parse::<i16>().unwrap()),
            10 => self.twenty_plus_receiving = Some(val.parse::<i16>().unwrap()),
            11 => self.forty_plus_receiving = Some(val.parse::<i16>().unwrap()),
            12 => self.receiving_first_downs = Some(val.parse::<i16>().unwrap()),
            13 => self.receiving_first_down_pct = Some(val.parse::<f32>().unwrap()),
            14 => self.receiving_fumbles = Some(val.parse::<i16>().unwrap()),
            _ => unimplemented!(),
        }
    }
}

#[derive(Queryable, Serialize)]
pub struct TeStatsModel {
    pub id: i32,
    pub rank: Option<i16>,
    pub player: Option<String>,
    pub team: Option<Team>,
    pub receiving_receptions: Option<i16>,
    pub receiving_yards: Option<i16>,
    pub avg_receiving_yards: Option<f32>,
    pub avg_receiving_yards_per_game: Option<f32>,
    pub receiving_touchdowns: Option<i16>,
    pub receiving_long: Option<String>,
    pub receiving_first_downs: Option<i16>,
    pub receiving_first_down_pct: Option<f32>,
    pub twenty_plus_receiving: Option<i16>,
    pub forty_plus_receiving: Option<i16>,
    pub receiving_fumbles: Option<i16>,
    pub inserted_ts: Option<SystemTime>,
}

#[derive(Debug, Clone, Insertable)]
#[table_name="wr_total_stats"]
pub struct WrStats {
    pub rank: Option<i16>,
    pub player: Option<String>,
    pub team: Option<Team>,
    pub receiving_receptions: Option<i16>,
    pub receiving_yards: Option<i16>,
    pub avg_receiving_yards: Option<f32>,
    pub avg_receiving_yards_per_game: Option<f32>,
    pub receiving_touchdowns: Option<i16>,
    pub receiving_long: Option<String>,
    pub receiving_first_downs: Option<i16>,
    pub receiving_first_down_pct: Option<f32>,
    pub twenty_plus_receiving: Option<i16>,
    pub forty_plus_receiving: Option<i16>,
    pub receiving_fumbles: Option<i16>,
}

impl Default for WrStats {
    fn default() -> Self {
        Self {
            rank: None,
            player: None,
            team: None,
            receiving_receptions: None,
            avg_receiving_yards: None,
            receiving_yards: None,
            avg_receiving_yards_per_game: None,
            receiving_touchdowns: None,
            receiving_long: None,
            receiving_first_downs: None,
            receiving_first_down_pct: None,
            twenty_plus_receiving: None,
            forty_plus_receiving: None,
            receiving_fumbles: None,
        }
    }
}

impl Mappable for WrStats {
    fn mapper(&mut self, val: &str, stat: usize) {
        match stat {
            0 => self.rank = Some(val.parse::<i16>().unwrap()),
            1 => self.player = Some(parse_player_name(val)),
            2 => self.team = Some(parse_players_team(val).parse::<Team>().unwrap()),
            3 => (), // Do nothing - this is just the position, which is redundant.
            4 => self.receiving_receptions = Some(val.parse::<i16>().unwrap()),
            5 => self.receiving_yards = Some(val.replace(",", "").parse::<i16>().unwrap()),
            6 => self.avg_receiving_yards = Some(val.parse::<f32>().unwrap()),
            7 => self.avg_receiving_yards_per_game = Some(val.parse::<f32>().unwrap()),
            8 => self.receiving_long = Some(val.parse::<String>().unwrap()),
            9 => self.receiving_touchdowns = Some(val.parse::<i16>().unwrap()),
            10 => self.twenty_plus_receiving = Some(val.parse::<i16>().unwrap()),
            11 => self.forty_plus_receiving = Some(val.parse::<i16>().unwrap()),
            12 => self.receiving_first_downs = Some(val.parse::<i16>().unwrap()),
            13 => self.receiving_first_down_pct = Some(val.parse::<f32>().unwrap()),
            14 => self.receiving_fumbles = Some(val.parse::<i16>().unwrap()),
            _ => unimplemented!(),
        }
    }
}

#[derive(Queryable, Serialize)]
pub struct WrStatsModel {
    pub id: i32,
    pub rank: Option<i16>,
    pub player: Option<String>,
    pub team: Option<Team>,
    pub receiving_receptions: Option<i16>,
    pub receiving_yards: Option<i16>,
    pub avg_receiving_yards: Option<f32>,
    pub avg_receiving_yards_per_game: Option<f32>,
    pub receiving_touchdowns: Option<i16>,
    pub receiving_long: Option<String>,
    pub receiving_first_downs: Option<i16>,
    pub receiving_first_down_pct: Option<f32>,
    pub twenty_plus_receiving: Option<i16>,
    pub forty_plus_receiving: Option<i16>,
    pub receiving_fumbles: Option<i16>,
    pub inserted_ts: Option<SystemTime>,
}
