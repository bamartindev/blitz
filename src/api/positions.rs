use actix_web::web::Json;
use actix_web::Result;

use db::models::{QbStatsModel, RbStatsModel, TeStatsModel, WrStatsModel};
use db::{fetch_all_qb_stats, fetch_all_rb_stats, fetch_all_te_stats, fetch_all_wr_stats};

// Route handler for getting the QB stats from the DB
pub fn get_qb_stats() -> Result<Json<Vec<QbStatsModel>>> {
    info!("Handling request to fetch all QB stats");
    Ok(Json(fetch_all_qb_stats()))
}

pub fn get_rb_stats() -> Result<Json<Vec<RbStatsModel>>> {
    info!("Handling request to fetch all RB stats");
    Ok(Json(fetch_all_rb_stats()))
}

pub fn get_wr_stats() -> Result<Json<Vec<WrStatsModel>>> {
    info!("Handling request to fetch all WR stats");
    Ok(Json(fetch_all_wr_stats()))
}

pub fn get_te_stats() -> Result<Json<Vec<TeStatsModel>>> {
    info!("Handling request to fetch all TE stats");
    Ok(Json(fetch_all_te_stats()))
}
