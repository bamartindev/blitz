mod positions;

#[macro_use]
extern crate log;

use std::env;

use actix_web::{web, App, HttpResponse, HttpServer, Responder};
use dotenv::dotenv;

fn index() -> impl Responder {
    info!("Index has been accessed!");
    HttpResponse::Ok().body("Hello World")
}

fn main() {
    dotenv().ok();
    env_logger::init();

    let bind_address =
        env::var("BIND_ADDRESS").expect("BIND_ADDRESS environment variable is not set!");

    info!("Launching API server...");
    HttpServer::new(move || {
        App::new().service(
            web::scope("/api/stats")
                .route("/", web::get().to(index))
                .route("/qb", web::get().to(positions::get_qb_stats))
                .route("/rb", web::get().to(positions::get_rb_stats))
                .route("/wr", web::get().to(positions::get_wr_stats))
                .route("/te", web::get().to(positions::get_te_stats)),
        )
    })
    .bind(&bind_address)
    .unwrap_or_else(|_| panic!("Could not bind server to address {}", &bind_address))
    .run()
    .unwrap();
}
