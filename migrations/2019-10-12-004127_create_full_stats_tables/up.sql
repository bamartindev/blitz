-- Your SQL goes here
CREATE TABLE qb_total_stats (
    id SERIAL PRIMARY KEY,
    rank SMALLINT,
    player VARCHAR,
    team VARCHAR,
    passing_completions SMALLINT,
    passing_attempts SMALLINT,
    passing_pct REAL,
    avg_attempts_per_game REAL,
    passing_yards SMALLINT,
    avg_passing_yards REAL,
    avg_yards_per_game REAL,
    passing_touchdowns SMALLINT,
    interceptions SMALLINT,
    passing_first_downs SMALLINT,
    passing_first_down_pct REAL,
    passing_long VARCHAR,
    twenty_plus_pass SMALLINT,
    forty_plus_pass SMALLINT,
    sack_count SMALLINT,
    passer_rating REAL,
    inserted_ts TIMESTAMP without time zone default (now() at time zone 'utc')
);

CREATE TABLE rb_total_stats (
    id SERIAL PRIMARY KEY,
    rank SMALLINT,
    player VARCHAR,
    team VARCHAR,
    rushing_attempts SMALLINT,
    avg_rushing_attempts_per_game REAL,
    rushing_yards SMALLINT,
    avg_rushing_yards REAL,
    avg_rushing_yards_per_game REAL,
    rushing_touchdowns SMALLINT,
    rushing_long VARCHAR,
    rushing_first_downs SMALLINT,
    rushing_first_down_pct REAL,
    twenty_plus_rush SMALLINT,
    forty_plus_rush SMALLINT,
    rushing_fumbles SMALLINT,
    inserted_ts TIMESTAMP without time zone default (now() at time zone 'utc')
);

CREATE TABLE wr_total_stats (
    id SERIAL PRIMARY KEY,
    rank SMALLINT,
    player VARCHAR,
    team VARCHAR,
    receiving_receptions SMALLINT,
    receiving_yards SMALLINT,
    avg_receiving_yards REAL,
    avg_receiving_yards_per_game REAL,
    receiving_touchdowns SMALLINT,
    receiving_long VARCHAR,
    receiving_first_downs SMALLINT,
    receiving_first_down_pct REAL,
    twenty_plus_receiving SMALLINT,
    forty_plus_receiving SMALLINT,
    receiving_fumbles SMALLINT,
    inserted_ts TIMESTAMP without time zone default (now() at time zone 'utc')
);

CREATE TABLE te_total_stats (
    id SERIAL PRIMARY KEY,
    rank SMALLINT,
    player VARCHAR,
    team VARCHAR,
    receiving_receptions SMALLINT,
    receiving_yards SMALLINT,
    avg_receiving_yards REAL,
    avg_receiving_yards_per_game REAL,
    receiving_touchdowns SMALLINT,
    receiving_long VARCHAR,
    receiving_first_downs SMALLINT,
    receiving_first_down_pct REAL,
    twenty_plus_receiving SMALLINT,
    forty_plus_receiving SMALLINT,
    receiving_fumbles SMALLINT,
    inserted_ts TIMESTAMP without time zone default (now() at time zone 'utc')
);